#All test files must start with test_
#All test functions must be written test_FUNCTIONNAME
#All test classes must start with Test

def plus_three(x):
    return x+3

def test_two_plus_three():
    assert plus_three(2) == 5