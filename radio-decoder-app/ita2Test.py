import re
import ita2_table

#def baudot_decode_from_bitstream()

sampleRttyBitStream = "0111010111001000111010111001000111010111001000100100001001000000100011110111001111111110010001101011001000000100011110111001111111110010001101011001000000100011110111001111111110010001101011010001011000101000010010001111"

#breaks bit stream into groups of 5 bits for char. assume complete 5bit chunks without lead blanks
char_list = re.findall('.....',sampleRttyBitStream)

#decipher char bit chuncks into ASCII char through ITA2 Table
output_str = ''
figureShift = False;
for char in char_list:
    if char == '11111':
        figureShift = False
    elif char == '11011':
        figureShift = True
    else:
        output_str += str(ita2_table.decode[char][int(figureShift)])

print output_str