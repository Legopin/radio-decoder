#
# Copyright 2014 Clayton Smith.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

#import numpy
#import sys
#from gnuradio import gr

# Control characters
# Codeword	    Oct	Dec	Hex	Abbr	Description
# 1010101011	000	0	00	NUL	    Null character
# 1011011011	001	1	01	SOH	    Start of Header
# 1011101101	002	2	02	STX	    Start of Text
# 1101110111	003	3	03	ETX	    End of Text
# 1011101011	004	4	04	EOT	    End of Transmission
# 1101011111	005	5	05	ENQ	    Enquiry
# 1011101111	006	6	06	ACK	    Acknowledgment
# 1011111101	007	7	07	BEL	    Bell
# 1011111111	010	8	08	BS	    Backspace
# 11101111      011	9	09	HT	    Horizontal Tab
# 11101         012 10	0A	LF	    Line feed
# 1101101111	013	11	0B	VT	    Vertical Tab
# 1011011101	014	12	0C	FF	    Form feed
# 11111         015 13	0D	CR	    Carriage return
# 1101110101	016	14	0E	SO	    Shift Out
# 1110101011	017	15	0F	SI	    Shift In
# 1011110111	020	16	10	DLE	    Data Link Escape
# 1011110101	021	17	11	DC1	    Device Control 1 (XON)
# 1110101101	022	18	12	DC2	    Device Control 2
# 1110101111	023	19	13	DC3	    Device Control 3 (XOFF)
# 1101011011	024	20	14	DC4	    Device Control 4
# 1101101011	025	21	15	NAK	    Negative Acknowledgement
# 1101101101	026	22	16	SYN	    Synchronous Idle
# 1101010111	027	23	17	ETB	    End of Trans. Block
# 1101111011	030	24	18	CAN	    Cancel
# 1101111101	031	25	19	EM	    End of Medium
# 1110110111	032	26	1A	SUB	    Substitute
# 1101010101	033	27	1B	ESC	    Escape
# 1101011101	034	28	1C	FS	    File Separator
# 1110111011	035	29	1D	GS	    Group Separator
# 1011111011	036	30	1E	RS	    Record Separator
# 1101111111	037	31	1F	US	    Unit Separator
# 1110110101	177	127	7F	DEL	    Delete

decode = {
    '1010101011' : '\x00',    '1011011011' : '\x01',
    '1011101101' : '\x02',    '1101110111' : '\x03',
    '1011101011' : '\x04',    '1101011111' : '\x05',
    '1011101111' : '\x06',    '1011111101' : '\x07',
    '1011111111' : '\x08',    '11101111'   : '\x09',
    '11101'      : '\x0A',    '1101101111' : '\x0B',
    '1011011101' : '\x0C',    '11111'      : '\x0D',
    '1101110101' : '\x0E',    '1110101011' : '\x0F',
    '1011110111' : '\x10',    '1011110101' : '\x11',
    '1110101101' : '\x12',    '1110101111' : '\x13',
    '1101011011' : '\x14',    '1101101011' : '\x15',
    '1101101101' : '\x16',    '1101010111' : '\x17',
    '1101111011' : '\x18',    '1101111101' : '\x19',
    '1110110111' : '\x1A',    '1101010101' : '\x1B',
    '1101011101' : '\x1C',    '1110111011' : '\x1D',
    '1011111011' : '\x1E',    '1101111111' : '\x1F',
    '1'          : ' ',       '111111111'  : '!',
    '101011111'  : '"',       '111110101'  : '#',
    '111011011'  : '$',       '1011010101' : '%',
    '1010111011' : '&',       '101111111'  : '\'',
    '11111011'   : '(',       '11110111'   : ')',
    '101101111'  : '*',       '111011111'  : '+',
    '1110101'    : ',',       '110101'     : '-',
    '1010111'    : '.',       '110101111'  : '/',
    '10110111'   : '0',       '10111101'   : '1',
    '11101101'   : '2',       '11111111'   : '3',
    '101110111'  : '4',       '101011011'  : '5',
    '101101011'  : '6',       '110101101'  : '7',
    '110101011'  : '8',       '110110111'  : '9',
    '11110101'   : ':',       '110111101'  : ';',
    '111101101'  : '<',       '1010101'    : '=',
    '111010111'  : '>',       '1010101111' : '?',
    '1010111101' : '@',       '1111101'    : 'A',
    '11101011'   : 'B',       '10101101'   : 'C',
    '10110101'   : 'D',       '1110111'    : 'E',
    '11011011'   : 'F',       '11111101'   : 'G',
    '101010101'  : 'H',       '1111111'    : 'I',
    '111111101'  : 'J',       '101111101'  : 'K',
    '11010111'   : 'L',       '10111011'   : 'M',
    '11011101'   : 'N',       '10101011'   : 'O',
    '11010101'   : 'P',       '111011101'  : 'Q',
    '10101111'   : 'R',       '1101111'    : 'S',
    '1101101'    : 'T',       '101010111'  : 'U',
    '110110101'  : 'V',       '101011101'  : 'W',
    '101110101'  : 'X',       '101111011'  : 'Y',
    '1010101101' : 'Z',       '111110111'  : '[',
    '111101111'  : '\\',      '111111011'  : ']',
    '1010111111' : '^',       '101101101'  : '_',
    '1011011111' : '`',       '1011'       : 'a',
    '1011111'    : 'b',       '101111'     : 'c',
    '101101'     : 'd',       '11'         : 'e',
    '111101'     : 'f',       '1011011'    : 'g',
    '101011'     : 'h',       '1101'       : 'i',
    '111101011'  : 'j',       '10111111'   : 'k',
    '11011'      : 'l',       '111011'     : 'm',
    '1111'       : 'n',       '111'        : 'o',
    '111111'     : 'p',       '110111111'  : 'q',
    '10101'      : 'r',       '10111'      : 's',
    '101'        : 't',       '110111'     : 'u',
    '1111011'    : 'v',       '1101011'    : 'w',
    '11011111'   : 'x',       '1011101'    : 'y',
    '111010101'  : 'z',       '1010110111' : '{',
    '110111011'  : '|',       '1010110101' : '}',
    '1011010111' : '~',       '1110110101' : '\x7F' }

