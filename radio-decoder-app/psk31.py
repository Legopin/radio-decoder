import re
import varicode
import numpy as np

from scikits.audiolab import Sndfile
from scipy.signal import butter, lfilter

def butter_lowpass_filter(data, cutoff, fs, order):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    return lfilter(b, a, data)

#takes in list with each item being bits for each charachter
def decode_varicode(char_list):
    output_str = ''
    for char in char_list:
        output_str += varicode.decode.get(char, '')
    return output_str

def demodulate(file_path):
    SYMBOL_RATE = 31.25
    f = Sndfile(file_path)

    samples_per_symbol = int(round(f.samplerate / SYMBOL_RATE))
    half = samples_per_symbol // 2

    sig = f.read_frames(f.nframes)
    delay = f.samplerate / SYMBOL_RATE
    delayed = np.hstack((np.zeros(delay, dtype='float64'), sig))
    transitions = delayed[:len(sig)]*sig

    filtered = butter_lowpass_filter(transitions, cutoff=600, fs=f.samplerate, order=3)
    digital = np.where(filtered > 0, 1, 0)

    # Sample the stream at half intervals, using the transition points as anchors
    bit_stream = []
    indices, = np.diff(digital).nonzero()

    for i in range(len(indices)-1):
        if indices[i+1] - indices[i] >= half:
            for index in range(indices[i]+half, indices[i+1], samples_per_symbol):
                bit_stream.append('%d' % digital[index])

    bit_stream = ''.join(bit_stream)
    char_list = re.split('00+', bit_stream)

    # Decode the characters
    output_str = decode_varicode(char_list)
    return output_str

def test_demodulate():
    #test method
    print ('Message:', demodulate('radio-psk31-bpsk.wav'))