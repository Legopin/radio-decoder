#module contains methods to export decoded messages to text file format
from datetime import datetime
from os import path

#takes string inputs save file path, source file path, Message
#Saves to text file with timestamp


def txt_file_export(file_full_path, source_file_path, message):
    f = open(file_full_path,'w')
    f.write(path.split(source_file_path)[1] + ' decoded on ' + datetime.now().strftime('%c') + '\n')
    f.write("Message Body:\n\n" + message)
    f.close()

msg = 'This is what I have in Mind\rThat no one shall be left behind!'
#txt_file_export("Users/Sean/Desktop/tester1.txt", 'audio/radio-psk31-bpsk.wav', msg )