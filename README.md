# README #

### What is this repository for? ###

This Python application is developed for CSCI 450 "Software Engineering" to gain experience in using development tools. The application is designed to decode common amateur(HAM) radio text communication protocols from audio into text.


### Contributors ###
* David Ko
* Alvin Suh
* William Tan
* Pin-Shan Yuan